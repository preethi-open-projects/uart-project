#include<stdio.h>
#include "app.h"
#include"main.h"
#include"gpio.h"
#include "usart.h"
#include<string.h>
#include<stdbool.h>

#define LED_BLINK_DELAY_MS 4000
static void _timer_delay_ms(uint32_t delay_ms);
static void _send_uart_data(uint8_t *data,uint16_t len);
///////////////////////////////////////////////////Public Functions
void app_start(void)
{
	HAL_GPIO_WritePin(LD1_GREEN_GPIO_Port, LD1_GREEN_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(LD3_BLUE_GPIO_Port, LD3_BLUE_Pin, GPIO_PIN_SET);

	while(true)
	{
		_timer_delay_ms(LED_BLINK_DELAY_MS);
		HAL_GPIO_TogglePin(LD1_GREEN_GPIO_Port, LD1_GREEN_Pin);
		_send_uart_data("Hello World\n", strlen("Hello World\n"));
	}
}

//////////////////////////////////////////////

static void _timer_delay_ms(uint32_t delay_ms)
{
	uint32_t tick_start = HAL_GetTick();

	while((HAL_GetTick()-tick_start) < delay_ms)
	{

	}
}

static void _send_uart_data(uint8_t *data,uint16_t len)
{
	if(!data)
		return;

	HAL_UART_Transmit(&huart1, data , len , 0xFFFF);
}
